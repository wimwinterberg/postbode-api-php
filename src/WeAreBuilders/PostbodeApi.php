<?php
namespace WeAreBuilders;

/**
 * Class PostbodeApi
 *
 * Based on:
 * - https://bitbucket.org/postbodenu/postbode-api-php Mark Hameetman <mark@bureaupartners.nl> (c) 2014 Postbode B.V.
 *
 * @package WeAreBuilders
 */
class PostbodeApi
{
    /**
     * Http status codes
     *
     */
    const HTTP_CODE_OK = 200;

    /**
     * Url methods
     *
     */
    const URL_METHOD_ADD  = 'postkantoor';
    const URL_METHOD_SEND = 'verzend';

    /**
     * Request params
     *
     */
    const REQUEST_PARAM_NAME     = 'name';
    const REQUEST_PARAM_DOCUMENT = 'document';
    const REQUEST_PARAM_TYPE     = 'type';
    const REQUEST_PARAM_ENVELOPE = 'envelope';

    /**
     * Response keys
     *
     */
    const RESPONSE_KEY_DOCUMENT = 'document';
    const RESPONSE_KEY_LETTER   = 'letter';
    const RESPONSE_KEY_NAME     = 'name';
    const RESPONSE_KEY_ADDED    = 'added';
    const RESPONSE_KEY_SEND     = 'send';

    /**
     * Account
     *
     * @var string
     */
    private $account;

    /**
     * Password
     *
     * @var string
     */
    private $password;

    /**
     * Posted data
     *
     * @var array
     */
    private $postData = array();

    /**
     * Post types
     *
     */
    const POST_TYPE_STANDARD      = 'standaard';
    const POST_TYPE_PREMIUM       = 'premium';
    const POST_TYPE_REGISTERED    = 'aangetekend';
    const POST_TYPE_INTERNATIONAL = 'internationaal';

    /**
     * Allowed post types
     *
     * @var array
     */
    private static $allowedPostTypes = array(
        self::POST_TYPE_STANDARD,
        self::POST_TYPE_PREMIUM,
        self::POST_TYPE_REGISTERED,
        self::POST_TYPE_INTERNATIONAL
    );

    /**
     * Response types
     *
     */
    const RESPONSE_TYPE_JSON = 'json';
    const RESPONSE_TYPE_XML  = 'xml';

    /**
     * Allowed response types
     *
     * @var array
     */
    private static $allowedResponseTypes = array(
        self::RESPONSE_TYPE_JSON,
        self::RESPONSE_TYPE_XML
    );

    /**
     * Response type
     *
     * @var string
     */
    private $responseType = self::RESPONSE_TYPE_JSON;

    /**
     * Response code
     *
     * @var string
     */
    private $responseCode;

    /**
     * Response
     *
     * @var string
     */
    private $response;

    /**
     * Envelope id
     *
     * @var int
     */
    private $envelopeID;

    /**
     * Post type
     *
     * @var string
     */
    private $postType;

    /**
     * Retrieve document name
     *
     * @var string
     */
    private $documentNameAdded;

    /**
     * Retrieve letter name sent
     *
     * @var string
     */
    private $letterNameSent;

    /**
     * Whether file was successful added
     *
     * @var bool
     */
    private $successFulAdded = false;

    /**
     * Last request method url
     *
     * @var string|null
     */
    private $lastRequestMethodUrl = null;

    /**
     * Errors
     *
     */
    const ERROR_CODE_INVALID_POST_TYPE = 1000;
    const ERROR_CODE_NO_ENVELOPE_ID    = 1001;
    const ERROR_CODE_FILE_NOT_FOUND    = 1002;

    /**
     * Request methods
     *
     */
    const HTTP_METHOD_POST = 'POST';
    const HTTP_METHOD_GET  = 'GET';

    /**
     * API url
     *
     * @var string
     */
    protected static $apiUrl = 'https://www.postbode.nu/api';

    /**
     * Agent
     *
     * @var string
     */
    protected static $userAgent = 'Postbode.nu PHP API Client 2.0';

    /**
     * Constructor
     *
     * @param string $account
     * @param string $password
     */
    public function __construct($account, $password)
    {
        $this->account  = $account;
        $this->password = $password;
    }

    /**
     * Sets the response type
     *
     * @param string $responseType
     * @return $this
     */
    public function setResponseType($responseType)
    {
        $responseType = strtolower($responseType);
        if (in_array($responseType, self::$allowedResponseTypes)) {
            $this->responseType = $responseType;
        } else {
            $this->responseType = self::RESPONSE_TYPE_JSON;
        }

        return $this;
    }

    /**
     * Retrieve response code
     *
     * @return string
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Retrieve response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Sets the envelope id
     *
     * @param int $envelopeId
     * @return $this
     */
    public function setEnvelopeID($envelopeId)
    {
        $this->envelopeID = $envelopeId;

        return $this;
    }

    /**
     * Retrieve envelope id
     *
     * @return int
     */
    public function getEnvelopeID()
    {
        return $this->envelopeID;
    }

    /**
     * Sets the post type
     *
     * @param string $postType
     * @return $this
     */
    public function setPostType($postType)
    {
        $postType = strtolower($postType);
        if (in_array($postType, self::$allowedPostTypes)) {
            $this->postType = $postType;
        } else {
            $this->postType = null;
        }

        return $this;
    }

    /**
     * Retrieve post type
     *
     * @return string
     */
    public function getPostType()
    {
        return $this->postType;
    }

    /**
     * Set data for letter
     *
     * @param string $fileName file to add
     * @param string $name     (Optional) Defaults to null. Name of the document. When null base filename will used as name
     * @return $this
     */
    protected function setDataForLetter($fileName, $name = null)
    {
        if (!file_exists($fileName)) {
            throw new \InvalidArgumentException(sprintf('Given file \'%1$s\' does not exists', $fileName), self::ERROR_CODE_FILE_NOT_FOUND);
        }

        // reset post data
        $this->postData = array();
        if ($name == null) {
            $name = basename($fileName);
        }

        // set name and filename
        $this->setPostData(self::REQUEST_PARAM_NAME, $name);
        $this->setPostData(self::REQUEST_PARAM_DOCUMENT, $this->encodeFile($fileName));
    }

    /**
     * Adds letter to concepts in Postbode.nu
     *
     * @param string $fileName file to add
     * @param string $name     (Optional) Defaults to null. Name of the document. When null base filename will used as name
     * @return $this
     */
    public function addLetter($fileName, $name = null)
    {
        $this->setDataForLetter($fileName, $name);
        $this->sendRequest(self::URL_METHOD_ADD, self::HTTP_METHOD_POST);

        return $this;
    }

    /**
     * Sends the letter directly via Postbode.nu
     *
     * @param string $fileName file to send
     * @param string $name     (Optional) Defaults to null. Name of the document. When null base filename will used as name
     * @return $this
     */
    public function sendLetter($fileName, $name = null)
    {
        $this->setDataForLetter($fileName, $name);

        if (in_array($this->getPostType(), self::$allowedPostTypes)) {
            $this->setPostData(self::REQUEST_PARAM_TYPE, $this->getPostType());
        } else {
            throw new \InvalidArgumentException(sprintf('Given post type \'%1$s\' is in valid', $this->getPostType()), self::ERROR_CODE_INVALID_POST_TYPE);
        }
        // for post type 'Standaard' only EnvelopeID 1 allowed
        if ($this->getPostType() == self::POST_TYPE_STANDARD) {
            $this->setEnvelopeID(1);
        }

        if (is_numeric($this->getEnvelopeID())) {
            $this->setPostData(self::REQUEST_PARAM_ENVELOPE, $this->getEnvelopeID());
        } else {
            throw new \InvalidArgumentException(sprintf('Given envelope id is invalid \'%1$s\' is in valid', $this->getEnvelopeID()), self::ERROR_CODE_NO_ENVELOPE_ID);
        }

        $this->sendRequest(self::URL_METHOD_SEND, self::HTTP_METHOD_POST);

        return $this;
    }

    /**
     * Perform the request
     *
     * @param string $methodUrl
     * @param string $type (Optional) Defaults to self::REQUEST_GET
     */
    protected function sendRequest($methodUrl, $type = self::HTTP_METHOD_GET)
    {
        $this->lastRequestMethodUrl = $methodUrl;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::$apiUrl . '/' . $methodUrl . '.' . $this->responseType);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->account . ':' . $this->password);
        curl_setopt($curl, CURLOPT_USERAGENT, self::$userAgent);
        if ($type == self::HTTP_METHOD_POST) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $this->postData);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $this->response     = curl_exec($curl);
        $this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $this->parseResponse();
    }

    /**
     * Set post data
     *
     * @param string $name
     * @param string $value
     */
    protected function setPostData($name, $value)
    {
        $this->postData[$name] = $value;
    }

    /**
     * Encode file
     *
     * @param string $fileName
     * @return string
     */
    protected function encodeFile($fileName)
    {
        return base64_encode(file_get_contents($fileName));
    }

    /**
     * Parse response
     *
     */
    protected function parseResponse()
    {
        // reset parsed results
        $this->documentNameAdded = null;
        $this->letterNameSent    = null;
        $this->successFulAdded   = false;

        if ($this->getResponseCode() == self::HTTP_CODE_OK) {
            $normalizedResponse = $this->getNormalizedResponse();
            if ($normalizedResponse !== null) {
                if (

                    // letter(send) or document(added) is set
                    (isset($normalizedResponse[self::RESPONSE_KEY_DOCUMENT]) || isset($normalizedResponse[self::RESPONSE_KEY_LETTER])) &&

                    // name of the pdf sent
                    isset($normalizedResponse[self::RESPONSE_KEY_NAME]) &&

                    // added or send
                    (isset($normalizedResponse[self::RESPONSE_KEY_ADDED]) || isset($normalizedResponse[self::RESPONSE_KEY_SEND]))
                ) {

                    if ($this->lastRequestMethodUrl == self::URL_METHOD_ADD) {
                        $this->successFulAdded   = (bool)$normalizedResponse[self::RESPONSE_KEY_ADDED];
                        $this->documentNameAdded = isset($normalizedResponse[self::RESPONSE_KEY_DOCUMENT]) ? $normalizedResponse[self::RESPONSE_KEY_DOCUMENT] : null;
                    } else if ($this->lastRequestMethodUrl == self::URL_METHOD_SEND) {
                        $this->successFulAdded = (bool)$normalizedResponse[self::RESPONSE_KEY_SEND];
                        $this->letterNameSent  = isset($normalizedResponse[self::RESPONSE_KEY_LETTER]) ? $normalizedResponse[self::RESPONSE_KEY_LETTER] : null;
                    }
                }
            }
        }
    }

    /**
     * Retrieve normalized response data
     *
     * @return array|null
     */
    protected function getNormalizedResponse()
    {
        $retValue = null;

        if ($this->responseType == self::RESPONSE_TYPE_JSON) {
            $retValue = json_decode($this->getResponse(), true);
        } else if ($this->responseType == self::RESPONSE_TYPE_XML) {
            $previousSettingLibXmlUseInternalErrors = libxml_use_internal_errors(true);

            $simpleXml = simplexml_load_string($this->getResponse());
            if ($simpleXml !== false) {
                $retValue = (array)$simpleXml;
            }
            libxml_use_internal_errors($previousSettingLibXmlUseInternalErrors);
        }

        return $retValue;
    }

    /**
     * Whether request was successful
     *
     * @return bool
     */
    public function isSuccess()
    {
        return $this->successFulAdded;
    }

    /**
     * Retrieve name of the document added
     *
     * @return string|null
     */
    public function getDocumentNameAdded()
    {
        return $this->documentNameAdded;
    }

    /**
     * Retrieve name of the letter sent
     *
     * @return string|null
     */
    public function getLetterNameSent()
    {
        return $this->letterNameSent;
    }
}